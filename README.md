[![status-badge](https://ci.codeberg.org/api/badges/BeoCode/Weather/status.svg)](https://ci.codeberg.org/BeoCode/Weather)
[![license](https://img.shields.io/badge/license-EUPL--1.2-blue)](https://codeberg.org/BeoCode/Weather/src/branch/main/LICENSE)
[![F-Droid version](https://img.shields.io/f-droid/v/de.beowulf.wetter)](https://f-droid.org/de/packages/de.beowulf.wetter/)
[![Download F-Droid](https://img.shields.io/badge/download-F--Droid-orange)](https://f-droid.org/de/packages/de.beowulf.wetter/)
[![Download PlayStore](https://img.shields.io/badge/download-PlayStore-green)](https://play.google.com/store/apps/details?id=de.beowulf.wetter)
[![Translation](https://img.shields.io/badge/translation-POEditor-pink)](https://poeditor.com/join/project?hash=WhLrkxFJUt)

# Weather

<img src="https://codeberg.org/BeoCode/Weather/raw/branch/main/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" height=75px> **Weather App with data from OpenWeatherMap**

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="60">](https://f-droid.org/de/packages/de.beowulf.wetter/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="60">](https://play.google.com/store/apps/details?id=de.beowulf.wetter)

## Features
- current weather
- 7 day forecast
- 48 hour forecast
- weather maps
- rain radar
- weather alerts
- multiple cities
- multiple units

## Supported languages:
- English (100%)
- Armenian (70%)
- Bosnian (94%)
- Bulgarian (94%)
- Chinese (simplified) (100%)
- Chinese (traditional) (100%)
- Czech (94%)
- Dutch (100%)
- Esperanto (94%)
- Finnish (94%)
- French (100%)
- Georgian (78%)
- German (100%)
- Greek (88%)
- Italian (94%)
- Macedonian (84%)
- Polish (100%)
- Portuguese (BR) (94%)
- Russian (94%)
- Serbian (Latin) (94%)
- Spanish (94%)
- Turkish (100%)
- Ukrainian (100%)

## Weather data
Weather data is provided by [OpenWeatherMap.org](https://openweathermap.org/), licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

## Get an API-Key
Go to [OpenWeatherMap](https://home.openweathermap.org/users/sign_up) and sign up for free. It may take a while before the API-Key is activated!

## Icons
[Icons](https://erikflowers.github.io/weather-icons/) by [E. Flowers](https://www.twitter.com/erik_flowers) &amp; [L. Bischoff](https://www.twitter.com/artill) licensed under [SIL OFL 1.1](https://scripts.sil.org/OFL).

## Contributing
See our [Contributing doc](CONTRIBUTING.md) for information on how to report issues or translate the app into your language.  
Many thanks to all [contributors](https://codeberg.org/BeoCode/Weather/wiki/Contributors).

## Wiki
You have questions, need help to set up the app or simply want to know more about it? Then take a look at the [Wiki](https://codeberg.org/BeoCode/Weather/wiki).

## Licensing
See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.
